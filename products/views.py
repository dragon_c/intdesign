from django.shortcuts import render
from .models import Category, Products
from django.http import JsonResponse
# Create your views here.


def category(request):
    categories = [{'title':item.title} for item in Category.objects.all()]
    return JsonResponse({'categories': categories})


def products(request):
    product = [{'title':item.title, 'category':item.category, 'price':item.price, 'image':item.image, 'text':item.text} for item in  Products.objects.all()]
    return JsonResponse({'product': product})