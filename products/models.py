from django.db import models

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name='Категорія')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категорія"
        verbose_name_plural = "Категорії"


class Products(models.Model):
    category = models.ForeignKey(Category, verbose_name='Категорія')
    price = models.IntegerField(default=0, verbose_name='Ціна')
    title = models.CharField(max_length=200, verbose_name='Назва продукту')
    image = models.ImageField(verbose_name='Зображення')
    text = models.TextField(verbose_name='Опис')

    def __str__(self):
        return '%s %s %s' %(self.title, '->', self.price)

    class Meta:
        verbose_name = "Продукти"
        verbose_name_plural = "Продукт"

